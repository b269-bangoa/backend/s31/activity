// 1. What directive is used by Node.js in loading the modules it needs?

//Answer: require() is used to load a module like http


// 2. What Node.js module contains a method for server creation?

//Answer: http module is used to create a server using Node.js

// 3. What is the method of the http object responsible for creating a server using Node.js?

//Answer: createServer() method is used to create a server using Node.js

// 4. What method of the response object allows us to set status codes and content types?
  
//Answer: response.writeHead() method is used to set the status and content type of objects in Node.js.

// 5. Where will console.log() output its contents when run in Node.js?

//Answer: The output will show in your terminal.

// 6. What property of the request object contains the address's endpoint?

//Answer: request.url is the property used to contain the address' endpoint




